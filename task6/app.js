const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
  }

  function addDataToEmployee(employee, newData) {
    return {...employee, ...newData};
  }

console.log(addDataToEmployee(employee, {age: 51, salary: 70000}));