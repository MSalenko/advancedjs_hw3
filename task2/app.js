const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];
  
  function shortenCharacters(characters) {
    // const charactersShortInfo = [];
    // characters.forEach(character => {
    //   const {name, lastName, age} = character;
    //   const characterShort = {name, lastName, age};
    //   charactersShortInfo.push(characterShort);
    // });
    
    // Пропозиція ментора: в другому завданні цього ж можна досягти за допомогою методу масивів map. Ось, зробив:
    const charactersShortInfo = characters.map(({name, lastName, age}) => ({name, lastName, age}));
    return charactersShortInfo;
  }
  
  console.log(shortenCharacters(characters));